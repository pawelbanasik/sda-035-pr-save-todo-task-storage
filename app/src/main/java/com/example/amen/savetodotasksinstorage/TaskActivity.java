package com.example.amen.savetodotasksinstorage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TaskActivity extends AppCompatActivity {

    private EditText content, title, date;
    private Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        content = (EditText) findViewById(R.id.inputContent);
        date = (EditText) findViewById(R.id.inputDate);
        title = (EditText) findViewById(R.id.inputTitle);

        saveBtn = (Button) findViewById(R.id.buttonSave);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // jeśli intent posiada extra Task,
                //stwórz task, a następnie dodaj go do listy i zapisz do pliku.
                // a następnie zamknij activity
                // if


                try {
                    if (!getIntent().hasExtra("Task")) {
                        FileManager.instance.getList().add(createToDoTask());
                        FileManager.instance.save(TaskActivity.this);
                    }
                    finish();
                } catch (ParseException e) {
                    Log.e(getClass().getName(), e.getMessage());
                }

            }
        });
        date.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
        if (getIntent().hasExtra("Task")){
            ToDoTask toDoTask = ToDoTask.parseLine(getIntent().getStringExtra("Task"));
            content.setText(toDoTask.getContent());
            // reszte dopisac
        }
        // powoduje schowanie klawiatury po uruchomieniu activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private ToDoTask createToDoTask() throws ParseException {
        // wykorzystaj pola interfejsu i stwórz z wartości pól obiekt ToDoTask
        //return new ToDoTask(titleString, contentString, currentDate);

        // parsuje na date
        Date data = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date.getText().toString());
        String tytul = title.getText().toString();
        String tresc = content.getText().toString();


        ToDoTask toDoTask = new ToDoTask(tytul, tresc, data);

        return toDoTask;
    }
}
