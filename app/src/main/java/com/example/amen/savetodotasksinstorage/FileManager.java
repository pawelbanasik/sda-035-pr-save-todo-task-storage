package com.example.amen.savetodotasksinstorage;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by amen on 1/16/17.
 */

public class FileManager {
    private static final String FILE_NAME = "todotasks.txt";

    public static final FileManager instance = new FileManager();
    private final List<ToDoTask> list;

    private FileManager() {
        list = new LinkedList<>();
    }

    public List<ToDoTask> getList() {
        return list;
    }

    // ładowanie do listy (która jest w pamięci aplikacji) wszystkich elementów z pliku
    public void load(Context context) {
        list.clear();
        list.addAll(getFromFile(context));
    }

    // zapisywanie listy do pliku.
    public void save(Context context) {
        saveToFile(context, list);
    }

    private void saveToFile(Context context, List<ToDoTask> listToSave) {
        // otwórz FileOutputStream i zapisz listę ToDoTask do pliku
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            PrintWriter printWriter = new PrintWriter(fileOutputStream);

            for (ToDoTask toDoTask : listToSave) {
                printWriter.println(toDoTask.toSerializedString());
            }
            printWriter.close();
        } catch (IOException e) {
            Log.e(getClass().getName(), e.getMessage());

        }

    }

    private List<ToDoTask> getFromFile(Context context) {
        List<ToDoTask> returnList = new LinkedList<>();
        // otwórz readera i załaduj ToDoTask z pliku do listy
        try {

            FileInputStream fis = context.openFileInput(FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            String line = bufferedReader.readLine();

            while (line != null) {


                ToDoTask toDoTask = ToDoTask.parseLine(line);
                returnList.add(toDoTask);

                line = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }

        return returnList;
    }
}