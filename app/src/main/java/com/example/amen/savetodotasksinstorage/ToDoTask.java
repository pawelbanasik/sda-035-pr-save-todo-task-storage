package com.example.amen.savetodotasksinstorage;

import android.support.annotation.VisibleForTesting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by amen on 1/16/17.
 */

public class ToDoTask {

    private static final String SEPARATOR = ";";

    protected Date data;
    private String tytul;
    private String tresc;


    public ToDoTask(String pTytul, String pTresc, Date pDate) {
        data = pDate;
        tytul = pTytul;
        tresc = pTresc;
    }

    public String toSerializedString() {
        return tytul
                + SEPARATOR + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(data)
                + SEPARATOR + tresc;
    }

    public String getTitle() {
        return tytul;
    }

    public String getContent() {
        return tresc;
    }

    public String getDateString() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(data);
    }

    public static ToDoTask parseLine(String fromLine) {
        String[] splits = fromLine.split(SEPARATOR);

        String tytul = splits[0];
        Date data = null;
        try {
            data = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(splits[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String tresc = "";
        try {
            tresc = splits[2];
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            aioobe.printStackTrace();
        }

        return new ToDoTask(tytul, tresc, data);
    }
}
